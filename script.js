class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set setName(name) {
        this.name = name;
    }

    get getName() {
        return this.name;
    }

    set setAge(age) {
        this.age = age;
    }

    get getAge() {
        return this.age;
    }

    set setSalary(salary) {
        this.salary = salary;
    }

    get getSalary() {
        return this.salary;
    }
}

const employee = new Employee('John', 29, 700);
console.log(employee);

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get getSalary() {
        return this.salary * 3;
    }

    get getLang() {
        return this.lang;
    }
}


const den = new Programmer('den', 29, 700, 'PHP');
console.log(den);
console.log('name: ', den.getName);

den.setName = 'Den';
console.log('name update: ', den.getName);

console.log('age: ', den.getAge);

den.setAge = den.getAge + 1;
console.log('age update: ', den.getAge);


console.log('salary: ', den.getSalary);

den.setSalary = 750;
console.log('salary update: ', den.getSalary);

console.log('Language: ', den.getLang);

const maria = new Programmer('Maria', 25, 800, 'JavaScript');
console.log(maria);

const julia = new Programmer('Julia', 31, 850, 'SEO');
console.log(julia);